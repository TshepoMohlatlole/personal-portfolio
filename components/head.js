import NextHead from 'next/head';
import { string } from 'prop-types';

const defaultMetaTitle = '';

const Head = (props) => (
    <NextHead>
        <meta charSet="UTF-8" />
        <title>{`${props.title} - Personal Portfolio` || defaultMetaTitle}</title>

        <link rel="icon" href="/favicon.ico" />
    
        <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
    </NextHead>
);

Head.propTypes = {
    title: string,
};

export default Head;